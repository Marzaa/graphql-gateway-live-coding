package com.wolkiewicz.graphql.registry.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wolkiewicz.graphql.registry.ServiceDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import java.net.InetAddress;
import java.net.UnknownHostException;

@Configuration
@ConfigurationProperties(prefix = "graphql.registry")
@ConditionalOnProperty("graphql.registry.uri")
public class GraphQLRegistryAutoConfiguration implements ApplicationListener<ApplicationReadyEvent> {
    private static final Logger LOG = LoggerFactory.getLogger(GraphQLRegistryAutoConfiguration.class);
    public static final MediaType JSON = MediaType.valueOf("application/json; charset=utf-8");

    private String uri;
    private final ApplicationContext applicationContext;
    private final Environment environment;

    public GraphQLRegistryAutoConfiguration(ApplicationContext applicationContext, Environment environment) {
        this.applicationContext = applicationContext;
        this.environment = environment;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        final String appName = applicationContext.getId();
        try {
            register(appName, uri);
        } catch (JsonProcessingException e) {
            LOG.error("ERRORS registering {} in {}", appName, uri, e);
        }
    }

    /**
     * Register the service in Graphql Gateway
     *
     * @param appName
     * @param uri
     * @return
     * @throws JsonProcessingException
     */
    private String register(String appName, String uri) throws JsonProcessingException {
        final WebClient webClient = WebClient.create();

        ServiceDto serviceDto = new ServiceDto();
        serviceDto.setName(appName);
        serviceDto.setUrl("http://" + getServiceHost() + ":" + environment.getProperty("local.server.port") + "/v2/api-docs");
        ObjectMapper objectMapper = new ObjectMapper();
        String body = objectMapper.writeValueAsString(serviceDto);

        return webClient
                .post()
                .uri(uri + "/registry")
                .header(HttpHeaders.CONTENT_TYPE, String.valueOf(JSON))
                .body(BodyInserters.fromValue(body)).retrieve()
                .bodyToMono(String.class)
                .block();

    }

    private String getServiceHost() {
        try {
            return InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
            LOG.error("Errors getting the host IP", e);
            return null;
        }
    }
}