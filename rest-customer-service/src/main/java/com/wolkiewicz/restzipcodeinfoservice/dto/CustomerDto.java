package com.wolkiewicz.restzipcodeinfoservice.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CustomerDto {
    private String name;
    private int age;
    private int count;
}
