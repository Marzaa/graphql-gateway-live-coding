package com.wolkiewicz.restzipcodeinfoservice.service;

import com.wolkiewicz.restzipcodeinfoservice.dto.CustomerDto;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

@Service
public class CustomersService {

    private final WebClient webClient;

    public CustomersService(WebClient.Builder webClientBuilder) {
        this.webClient = webClientBuilder.baseUrl("https://api.agify.io").build();
    }

    public CustomerDto someRestCall(String name) {
        return this.webClient
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path("/")
                        .queryParam("name", name)
                        .build())
                .retrieve()
                .bodyToMono(CustomerDto.class)
                .block();
    }
}
