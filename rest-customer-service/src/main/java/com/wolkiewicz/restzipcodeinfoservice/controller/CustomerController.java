package com.wolkiewicz.restzipcodeinfoservice.controller;

import com.wolkiewicz.restzipcodeinfoservice.dto.CustomerDto;
import com.wolkiewicz.restzipcodeinfoservice.service.CustomersService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(
        value = "/customers",
        produces = MediaType.APPLICATION_JSON_VALUE,
        headers = "Accept=" + MediaType.APPLICATION_JSON_VALUE
)
public class CustomerController {

    private final CustomersService customersService;

    public CustomerController(CustomersService customersService) {
        this.customersService = customersService;
    }

    @GetMapping("/")
    public String index() {
        return "Greetings from Spring Boot!";
    }

    @GetMapping("/{name}")
    public CustomerDto customerById(@PathVariable("name") String id) {
        return customersService.someRestCall(id);
    }
}
