package com.wolkiewicz.graphql.registry;

import lombok.Data;

@Data
public class ServiceDto {
    private String name;
    private String url;
}