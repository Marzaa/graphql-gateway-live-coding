package com.wolkiewicz.restzipcodeinfoservice;

import com.wolkiewicz.graphql.registry.client.GraphQLRegistryService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@GraphQLRegistryService
public class ZipCodeInfoAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZipCodeInfoAppApplication.class, args);
	}

}
