package com.wolkiewicz.restzipcodeinfoservice.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PlaceDto {
//    @JsonProperty("place name")
    private String placeName;
    private String longitude;
    private String state;
//    @JsonProperty("state abbreviation")
    private String stateAbbreviation;
    private String latitude;
}
