package com.wolkiewicz.restzipcodeinfoservice.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class ZipCodeInfoDto {
//    @JsonProperty("post code")
    private String postCode;
    private String country;
//    @JsonProperty("country abbreviation")
    private String countryAbbreviation;
    private List<PlaceDto> places;
}
