package com.wolkiewicz.restzipcodeinfoservice.controller;

import com.wolkiewicz.restzipcodeinfoservice.dto.ZipCodeInfoDto;
import com.wolkiewicz.restzipcodeinfoservice.service.ZipCodeInfoService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(
        value = "/zipcodeinfo",
        produces = MediaType.APPLICATION_JSON_VALUE,
        headers = "Accept=" + MediaType.APPLICATION_JSON_VALUE
)
public class ZipCodeController {

    private final ZipCodeInfoService zipCodeInfoService;

    public ZipCodeController(ZipCodeInfoService zipCodeService) {
        this.zipCodeInfoService = zipCodeService;
    }

    @GetMapping("/")
    public String index() {
        return "Greetings from Spring Boot!";
    }

    @GetMapping("/{postCode}")
    public ZipCodeInfoDto zipCodeInfoByPostCode(@PathVariable("postCode") String id) {
        return zipCodeInfoService.someRestCall(id);
    }
}
