package com.wolkiewicz.restzipcodeinfoservice.service;

import com.wolkiewicz.restzipcodeinfoservice.dto.ZipCodeInfoDto;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

@Service
public class ZipCodeInfoService {

    private final WebClient webClient;

    public ZipCodeInfoService(WebClient.Builder webClientBuilder) {
        this.webClient = webClientBuilder.baseUrl("https://api.zippopotam.us/pl/").build();
    }

    public ZipCodeInfoDto someRestCall(String postCode) {
        return this.webClient
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path("/" + postCode)
                        .build())
                .retrieve()
                .bodyToMono(ZipCodeInfoDto.class)
                .block();
    }
}
