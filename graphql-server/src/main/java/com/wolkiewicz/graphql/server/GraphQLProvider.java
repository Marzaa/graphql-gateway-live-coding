package com.wolkiewicz.graphql.server;

import com.wolkiewicz.graphql.schema.SwaggerGraphQLSchemaBuilder;
import graphql.GraphQL;
import io.swagger.models.Swagger;
import io.swagger.parser.SwaggerParser;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Component
public class GraphQLProvider {

    private Map<String, Swagger> services = new HashMap<>();
    private GraphQL graphQL;

    // register a rest service

    public void register(String name, String location) {
        services.put(name, new SwaggerParser().read(location));
        load();
    }

    //  loads rest services in graphql schema
    private void load() {
        SwaggerGraphQLSchemaBuilder graphQLConverter = new SwaggerGraphQLSchemaBuilder();
        services.values().stream().forEach(swagger -> graphQLConverter.swagger(swagger));
        this.graphQL = GraphQL.newGraphQL(graphQLConverter.build()).build();
    }

    // unregister a rest service
    public void unregister(String name) {
        services.remove(name);
        load();
    }

    public Collection<String> services() {
        return services.keySet();
    }

    public GraphQL getGraphQL() {
        return graphQL;
    }
}
