package com.wolkiewicz.graphql.server;

import lombok.Data;

@Data
public class GraphQLRequestBody {
    private String query;
}
